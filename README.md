# Exam Tools - Scan worker

Process PDF documents looking for Exam Tools barcodes

* Process scanned documents as well as programmatic PDF
* Process multi page documents
* Extraction of pages with barcodes
* Sorting of pages according to barcode
* Send documents to Exam Tools server (API calls)


## Example deployment

As post-upload hook of an FTP server, e.g. [Pure-FTPd](https://www.pureftpd.org/).

TODO: example config


## Configuration

You need to provide the secret key from your Exam Tools installation.


## Command line options

TODO


## Testing

Simply provide the ```--dry-run``` flag to avoid saving result to the Exam Tools
instance
